;;; my-misc.el --- Miscellaneous function definitions and settings
;;
;; Copyright (C) 2014-2024 Lluís Vilanova
;;
;; Author: Lluís Vilanova <vilanova@ac.upc.edu>
;;
;; This program is free software; you can redistribute it and/or
;; modify it under the terms of the GNU General Public License as
;; published by the Free Software Foundation, either version 3 of the
;; License, or (at your option) any later version.

;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see http://www.gnu.org/licenses/.

;;; Commentary:
;;
;; Miscellaneous function definitions and settings.

;;; Code:


;; * Functions

(defun my-run-async (program args callback)
  "Asynchronously run the given PROGRAM with arguments ARGS and invoke CALLBACK
  once finished."
  (let ((buffer (get-buffer-create (format "*%s*" program))))
    (unless (get-buffer-process buffer)
      (with-current-buffer buffer
        (erase-buffer))
      (let ((process (start-process-shell-command
                      program buffer (format "%s %s" program args))))
        (set-process-sentinel process callback)
        nil))))

(defun my-delay-function (func)
  "Delay execution of FUNC until next user's command."
  (let ((sym (intern (concat "my-hook-once--" (symbol-name func)))))
    (eval (macroexpand `(defun ,sym ()
                          (remove-hook 'pre-command-hook ',sym)
                          (,func))))
    (add-hook 'pre-command-hook sym)))


;; * Buffers

(defun rename-current-buffer-file ()
  "Renames current buffer and file it is visiting."
  (interactive)
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not (and filename (file-exists-p filename)))
        (error "Buffer '%s' is not visiting a file!" name)
      (let ((new-name (read-file-name "New name: " filename)))
        (if (get-buffer new-name)
            (error "A buffer named '%s' already exists!" new-name)
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil)
          (message "File '%s' successfully renamed to '%s'"
                   name (file-name-nondirectory new-name)))))))

(defun delete-current-buffer-file ()
  "Removes file connected to current buffer and kills buffer."
  (interactive)
  (let ((filename (buffer-file-name))
        (buffer (current-buffer))
        (name (buffer-name)))
    (if (not (and filename (file-exists-p filename)))
        (ido-kill-buffer)
      (when (yes-or-no-p "Are you sure you want to remove this file? ")
        (delete-file filename)
        (kill-buffer buffer)
        (message "File '%s' successfully removed" filename)))))

(defun copy-current-buffer-file-name-to-clipboard ()
  "Copy the current buffer file name to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (when filename
      (kill-new filename)
      (message "Copied buffer file name '%s' to the clipboard." filename))))

(defun my-dwim-dired-jump (&optional other-window)
  "If in a project, jump to its root directory, otherwise call `dired-jump'."
  (interactive "P")
  (dired-jump other-window
              (and (ede-current-project)
                   (ede-project-root-directory (ede-current-project)))))


;; * Windows

(defun rotate-windows ()
  "Rotate your windows."
  (interactive)
  (cond ((not (> (count-windows)1))
         (message "You can't rotate a single window!"))
        (t
         (setq i 1)
         (setq numWindows (count-windows))
         (while  (< i numWindows)
           (let* (
                  (w1 (elt (window-list) i))
                  (w2 (elt (window-list) (+ (% i numWindows) 1)))

                  (b1 (window-buffer w1))
                  (b2 (window-buffer w2))

                  (s1 (window-start w1))
                  (s2 (window-start w2))
                  )
             (set-window-buffer w1  b2)
             (set-window-buffer w2 b1)
             (set-window-start w1 s2)
             (set-window-start w2 s1)
             (setq i (1+ i)))))))

(defun toggle-window-split ()
  "Toggle between vertical/horizontal splits."
  (interactive)
  (if (= (count-windows) 2)
      (let* ((this-win-buffer (window-buffer))
             (next-win-buffer (window-buffer (next-window)))
             (this-win-edges (window-edges (selected-window)))
             (next-win-edges (window-edges (next-window)))
             (this-win-2nd (not (and (<= (car this-win-edges)
                                         (car next-win-edges))
                                     (<= (cadr this-win-edges)
                                         (cadr next-win-edges)))))
             (splitter
              (if (= (car this-win-edges)
                     (car (window-edges (next-window))))
                  'split-window-horizontally
                'split-window-vertically)))
        (delete-other-windows)
        (let ((first-win (selected-window)))
          (funcall splitter)
          (if this-win-2nd (other-window 1))
          (set-window-buffer (selected-window) this-win-buffer)
          (set-window-buffer (next-window) next-win-buffer)
          (select-window first-win)
          (if this-win-2nd (other-window 1))))))

(defun my-swap-window-buffers-by-dnd (drag-event)
  "Swaps the buffers displayed in the DRAG-EVENT's start and end window."
  (interactive "e")
  (let ((start-win (cl-caadr drag-event))
        (end-win   (cl-caaddr drag-event)))
    (when (and (windowp start-win)
               (windowp end-win)
               (not (eq start-win end-win))
               (not (memq (minibuffer-window)
                          (list start-win end-win))))
      (let ((bs (window-buffer start-win))
            (be (window-buffer end-win)))
        (unless (eq bs be)
          (set-window-buffer start-win be)
          (set-window-buffer end-win bs))))))


;; * Editing

(defun my-move-beginning-of-line (arg)
  "Move point back to indentation of beginning of line.

Move point to the first non-whitespace character on this line.
If point is already there, move to the beginning of the line.
Effectively toggle between the first non-whitespace character and
the beginning of the line.

If ARG is not nil or 1, move forward ARG - 1 lines first.  If
point reaches the beginning or end of the buffer, stop there."
  (interactive "^p")
  (setq arg (or arg 1))

  ;; Move lines first
  (when (/= arg 1)
    (let ((line-move-visual nil))
      (forward-line (1- arg))))

  (let ((orig-point (point)))
    (back-to-indentation)
    (when (= orig-point (point))
      (move-beginning-of-line 1))))

(defun my-smart-open-line ()
  "Insert an empty line after the current line.
Position the cursor at its beginning, according to the current mode."
  (interactive)
  (move-end-of-line nil)
  (newline-and-indent))

(defun my-smart-open-line-above ()
  "Insert an empty line above the current line.
Position the cursor at it's beginning, according to the current mode."
  (interactive)
  (move-beginning-of-line nil)
  (newline-and-indent)
  (forward-line -1)
  (indent-according-to-mode))

(defun my-eval-and-replace ()
  "Replace the preceding sexp with its value."
  (interactive)
  (backward-kill-sexp)
  (condition-case nil
      (prin1 (eval (read (current-kill 0)))
             (current-buffer))
    (error (message "Invalid expression")
           (insert (current-kill 0)))))


;; * Applications

;; `shell'
(defun comint-delchar-or-eof-or-kill-buffer (arg)
  (interactive "p")
  (if (null (get-buffer-process (current-buffer)))
      (kill-buffer)
    (comint-delchar-or-maybe-eof arg)))
(add-hook 'shell-mode-hook
          (lambda ()
            (define-key shell-mode-map
              (kbd "C-d") 'comint-delchar-or-eof-or-kill-buffer)))

;; `eshell'
(defun my-eshell (mode to-current-dir)
  (if (string= "eshell-mode" major-mode)
      (jump-to-register :my-eshell-window-config)
    (let ((dir default-directory))
      (window-configuration-to-register :my-eshell-window-config)
      (case mode
        ('fullscreen
         (call-interactively 'eshell)
         (delete-other-windows))
        ('other-window
         (if (one-window-p)
             (split-window)
           (other-window 1))
         (call-interactively 'eshell))
        (t (error "Unknown my-esshell mode: %s" mode)))
      (when to-current-dir
        (eshell/cd dir)
        (eshell-send-input)))))

;; `wgrep'
(use-package wgrep
  :ensure t
  :commands wgrep-toggle-readonly-area
  :bind (:map grep-mode-map
              ("C-c C-p" . wgrep-toggle-readonly-area)))

;;;; `csv-mode': visualize/edit CSV-formatted files
(use-package csv-mode
  :ensure t
  :config
  (progn
    (require 'color)
    (defun csv-highlight (&optional separator)
      (interactive (list (when current-prefix-arg (read-char "Separator: "))))
      (font-lock-mode 1)
      (let* ((separator (or separator ?\,))
             (n (count-matches (string separator) (pos-bol) (pos-eol)))
             (colors (cl-loop for i from 0 to 1.0 by (/ 2.0 n)
                              collect (apply #'color-rgb-to-hex
                                             (color-hsl-to-rgb i 0.3 0.5)))))
        (cl-loop for i from 2 to n by 2
                 for c in colors
                 for r = (format "^\\([^%c\n]+%c\\)\\{%d\\}" separator separator i)
                 do (font-lock-add-keywords nil `((,r (1 '(face (:foreground ,c)))))))))
    (add-hook 'csv-mode-hook 'csv-highlight)
    (add-hook 'csv-mode-hook 'csv-align-mode)
    (add-hook 'csv-mode-hook '(lambda () (interactive) (toggle-truncate-lines nil)))))

(provide 'my-misc)
;;; my-misc.el ends here
