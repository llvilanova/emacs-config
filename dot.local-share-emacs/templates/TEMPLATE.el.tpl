;;; TEMPLATE.el.tpl --- (>>>POINT<<<)

;; Copyright (>>>YEAR<<<) (>>>USER_NAME<<<)
;;
;; Author: (>>>USER_NAME<<<) <(>>>AUTHOR<<<)>
;; Keywords: (>>>1<<<)
;; X-URL: not distributed yet

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;;; Commentary:

;; (>>>2<<<)

;; Put this file into your load-path and the following into your ~/.emacs:
;;   (require '(>>>FILE_SANS<<<))

;;; Code:

(provide '(>>>FILE_SANS<<<))
(eval-when-compile
  (require 'cl))



;;; Classes

(>>>3<<<)


;;; Methods

;;; (>>>FILE<<<) ends here
